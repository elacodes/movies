<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType {
	
	public function __construct(array $subjects, array $authors)
	{
		$this->subjects = $subjects;
		$this->authors = $authors;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options) {
		
		$builder->add('title', 'text');
		$builder->add('poster', 'text');
		$builder->add('subjectId', 'choice', array(
				'choices' => $this->subjects,//$options['data']['subjects'],
				'placeholder' => 'Choose book Subject',
		));
		$builder->add('authorId', 'choice', array(
				'choices' => $this->authors,//$options['data']['authors'],
				'placeholder' => 'Choose book Author',
		));
		$builder->add('poster','file', array('required' => true, 'data_class' => null));
		$builder->add('overview','textarea');
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'AppBundle\Entity\booktown\books',
		));
	}
	
	public function getName() {
		return 'books';
	}
}

