<?php
namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\booktown\books;
use AppBundle\Entity\booktown\subjects;
use AppBundle\Entity\Document;
use AppBundle\Entity\booktown\authors;
use Doctrine\ORM\EntityRepository;
use AppBundle\Form\Type\BookType;

class BookController extends Controller 
{
	
	/**
	 * Lists all Book entities.
     * @Route("/admin/", name="admin_index")
     * @Route("/admin/", name="admin_book_index")
     * @Method("GET")
	 */
	public function indexAction()
	{
		$entityManager = $this->getDoctrine()->getManager();
		$books = $entityManager->getRepository('AppBundle:booktown\books')->findAll();
		return $this->render('admin/books/index.html.twig', array('books' => $books));
	}
	
	/**
	 * @Route("/admin/create-book", name="create_books")
	 * @Method({"GET", "POST"})
	 */
	public function createAction(Request $request) {
		$book = new books();
		
		$subjects = $this->getDoctrine()->getRepository('AppBundle:booktown\subjects')->findAllSubjects();
		$authors = $this->getDoctrine()->getRepository('AppBundle:booktown\authors')->findAllAuthors();

		$form = $this->createForm(new BookType($subjects, $authors), $book)
				->add('saveAndCreateNew', 'submit');
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			
			$posterName = self::slugify($book->getTitle());
				
			// $file stores the uploaded PDF file
			/** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
			$file = $book->getPoster();
				
			// Generate a unique name for the file before saving it
			$fileName = $posterName.'.'.$file->guessExtension();
				
			// Move the file to the directory where brochures are stored
			$bookCoverDir = $this->container->getParameter('kernel.root_dir').'/../web/uploads/BookCovers';
			$file->move($bookCoverDir, $fileName);
				
			// Update the 'brochure' property to store the PDF file name
			// instead of its contents
			$book->setPoster($fileName);
				
			$entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();
			$this->addFlash('success', 'book.created_successfully');
			
			// Flash messages are used to notify the user about the result of the
			// actions. They are deleted automatically from the session as soon
			// as they are accessed.
			// See http://symfony.com/doc/current/book/controller.html#flash-messages
			$this->addFlash('success', 'Created Book id '.$book->getId());
		
			if ($form->get('saveAndCreateNew')->isClicked()) {
				return $this->redirectToRoute('admin_book_new');
			}
		
			return $this->redirectToRoute('admin_book_index');
		}
		
		return $this->render('admin/books/new.html.twig', array(
				'book' => $book,
				'form' => $form->createView(),
		));
		
	}
	
	/**
	 * Finds and displays a Book entity. the getById automatically picks it up from here I guess	
	 *
	 * @Route("/admin/{id}", requirements={"id" = "\d+"}, name="admin_book_show")
	 * @Method("GET")
	 */
	public function showAction(books $book)	
	{
		$deleteForm = $this->createDeleteForm($book);
		
		return $this->render('admin/books/show_book_details.html.twig', array(
				'book'        => $book,
				'delete_form' => $deleteForm->createView(),
		));
	}
	
	/**
	 * Displays a form to edit an existing Book entity.
	 *
	 * @Route("/{id}/edit", requirements={"id" = "\d+"}, name="admin_book_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction(books $book, Request $request)
	{
		$subjects = $this->getDoctrine()->getRepository('AppBundle:booktown\subjects')->findAllSubjects();
		$authors = $this->getDoctrine()->getRepository('AppBundle:booktown\authors')->findAllAuthors();
		$entityManager = $this->getDoctrine()->getManager();
		$editForm = $this->createForm(new BookType($subjects, $authors), $book);
		$deleteForm = $this->createDeleteForm($book);
		$editForm->handleRequest($request);
		
		if ($editForm->isSubmitted() && $editForm->isValid()) {
			$posterName = self::slugify($book->getTitle());
			
			// $file stores the uploaded PDF file
			/** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
			$file = $book->getPoster();
			
			// Generate a unique name for the file before saving it
			$fileName = $posterName.'.'.$file->guessExtension();
			
			// Move the file to the directory where brochures are stored
			$bookCoverDir = $this->container->getParameter('kernel.root_dir').'/../web/uploads/BookCovers';
			$file->move($bookCoverDir, $fileName);
			
			// Update the 'brochure' property to store the PDF file name
			// instead of its contents
			$book->setPoster($fileName);
			
			$entityManager->flush();
			$this->addFlash('success', 'book.updated_successfully');
			return $this->redirectToRoute('admin_book_edit', array('id' => $book->getId()));
		}
		return $this->render('admin/books/edit.html.twig', array(
				'book'        => $book,
				'edit_form'   => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),
		));
	}
	
	/**
	 * @param string $string
	 *
	 * @return string
	 */
	public static function slugify($string)
	{
		return trim(preg_replace('/[^a-z0-9]+/', '-', strtolower(strip_tags($string))), '-');
	}
	
	/**
	 * @Route("/create-success", name="books-success")
	 */	
	public function successAction() {
		return $this->render('books/create-success.html.twig', array());
	}
	

	/**
	 * Creates a form to delete a Book entity by id.
	 *
	 * This is necessary because browsers don't support HTTP methods different
	 * from GET and POST. Since the controller that removes the blog posts expects
	 * a DELETE method, the trick is to create a simple form that *fakes* the
	 * HTTP DELETE method.
	 * See http://symfony.com/doc/current/cookbook/routing/method_parameters.html.
	 *
	 * @param Book $book The books object
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm(Books $book)
	{
		return $this->createFormBuilder()
		->setAction($this->generateUrl('admin_book_delete', array('id' => $book->getId())))
		->setMethod('DELETE')
		->getForm();
	}
	
	/**
	 * Deletes a book entity.
	 *
	 * @Route("/{id}", name="admin_book_delete")
	 * @Method("DELETE")
	 *
	 * The Security annotation value is an expression (if it evaluates to false,
	 * the authorization mechanism will prevent the user accessing this resource).
	 * The isAuthor() method is defined in the AppBundle\Entity\Post entity.
	 */
	public function deleteAction(Request $request, books $books)
	{
		$form = $this->createDeleteForm($books);
		$form->handleRequest($request);
	
		if ($form->isSubmitted() && $form->isValid()) {
			$entityManager = $this->getDoctrine()->getManager();
	
			$entityManager->remove($post);
			$entityManager->flush();
	
			$this->addFlash('success', 'book.deleted_successfully');
		}
	
		return $this->redirectToRoute('admin_book_index');
	}
}