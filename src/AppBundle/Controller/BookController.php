<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\booktown\books;
use AppBundle\Entity\booktown\subjects;
use AppBundle\Entity\booktown\authors;
use Doctrine\ORM\EntityRepository;

class BookController extends Controller 
{
	/**
	 * @Route("/", name="books_space_index")
	 */
	public function indexAction() {
		$entityManager = $this->getDoctrine()->getManager();
		$books = $entityManager->getRepository('AppBundle:booktown\books')->findAll();
		return $this->render('books/index.html.twig', array('home_books' => $books));
	}
	
	/**
	 * @Route("/books/top-rated", name="books_show")
	 */
	public function showTopRatedAction() {
		$books = $this->getDoctrine()->getConnection();
		
		$books = $this->getDoctrine()
					->getRepository('AppBundle:booktown\books')
					->findBy(array(), array('id'=>'DESC'), 6, null);
		
		if (!$books) {
			throw $this->createNotFoundException(
					'No books found'
			);
		}
		return $this->render('books/top-rated.html.twig', array('bookstore' => $books));
	}
	
	/**
	 * Finds and displays a Book entity. the getById automatically picks it up from here I guess
	 *
	 * @Route("/details/{id}", requirements={"id" = "\d+"}, name="book_details_show")
	 */
	public function showAction(books $book)
	{
		return $this->render('books/book_details.html.twig', array(
				'book'        => $book
		));
	}
	
	public function showGoogleBooks() {
		$client =new client();
		//books.google.com/books?uid=112569419106585232562&as_coll=1001&source=gbs_lp_bookshelf_list
		$response = $client->get("https://www.googleapis.com/books/v1/volumes?q=flowers+inauthor:keyes&key=");
		$data=$response->json();
		
		$items=$data['items'];
	}
}