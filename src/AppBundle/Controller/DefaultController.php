<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
	/**
	 * @Route("/{name}", name="homepage")
	 */
    public function indexAction($name="Elavarasi Julius Ceasar")
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array('name' => $name));
    }
}
