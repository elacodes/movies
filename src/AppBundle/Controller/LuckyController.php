<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class LuckyController extends Controller
{
	/**
	 * @Route("/lucky/{count}")
	 */
	public function numberAction($count)
	{
		$number = rand(0,10);
		$number = array();
		
		for ($i=0; $i < $count; $i++) {
			$number[] = rand(0,100);
		}
		
		$numberList = implode(' || ', $number);

		return $this->render('lucky/number.html.twig', array('luckyNumberList' => $numberList));
	}
}