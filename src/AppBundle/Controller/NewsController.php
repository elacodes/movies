<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class NewsController extends Controller 
{	
	public function recentNewsAction($max = 3)
	{
		// make a database call or other logic
		// to get the "$max" most recent articles
		$most_recent_news = array(
				array(
					'date' => '12.04.09',
					'title' => "Disney's A Christmas Carol",
					'snippet' => "&quot;Disney's A Christmas Carol,&quot; a multi-sensory thrill ride re-envisioned by Academy Award&reg;-winning filmmaker Robert Zemeckis, captures... ", 
				),
				array(
					'date' => '11.04.09',
					'title' => "Where the Wild Things Are",
					'snippet' => "Innovative director Spike Jonze collaborates with celebrated author Maurice Sendak to bring one of the most beloved books of all time to the big screen in &quot;Where the Wild Things Are,&quot;...",
				),
				array(
					'date' => '10.04.09',
					'title' => "The Box",
					'snippet' => "Norma and Arthur Lewis are a suburban couple with a young child who receive an anonymous gift bearing fatal and irrevocable consequences.",
				),
		);
	
		return $this->render(
				'news/most-recent-news.html.twig',
				array('most_recent_news' => $most_recent_news)
		);
	}
}