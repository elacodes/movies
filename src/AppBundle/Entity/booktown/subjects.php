<?php

namespace AppBundle\Entity\booktown;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * subjects
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\booktown\subjectsRepository")
 */
class subjects
{
	/**
	 * @ORM\OneToMany(targetEntity="books", mappedBy="subjects")
	 */
	protected $books;
	
	public function __construct()
	{
		$this->books = new ArrayCollection();
	}
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text")
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="text")
     */
    private $location;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return subjects
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return subjects
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }
    

    /**
     * Add books
     *
     * @param \AppBundle\Entity\booktown\books $books
     * @return subjects
     */
    public function addBook(\AppBundle\Entity\booktown\books $books)
    {
        $this->books[] = $books;

        return $this;
    }

    /**
     * Remove books
     *
     * @param \AppBundle\Entity\booktown\books $books
     */
    public function removeBook(\AppBundle\Entity\booktown\books $books)
    {
        $this->books->removeElement($books);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBooks()
    {
        return $this->books;
    }
}
