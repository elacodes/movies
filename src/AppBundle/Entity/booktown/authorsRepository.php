<?php

namespace AppBundle\Entity\booktown;

use Doctrine\ORM\EntityRepository;

/**
 * authorsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class authorsRepository extends EntityRepository
{
	public function findAllAuthors()
	{
		$results = $this->getEntityManager()
		->createQuery(
				'SELECT a.id, a.firstName, a.lastName FROM AppBundle:booktown\authors a ORDER BY a.lastName ASC'
		)
		->getResult();
		foreach ($results as $author) {
			$authors[$author['id']] = $author['firstName'] .' '. $author['lastName'];
		}
		return $authors;
	}
}
