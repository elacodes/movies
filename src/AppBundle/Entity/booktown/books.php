<?php

namespace AppBundle\Entity\booktown;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * books
 *
 * @ORM\Table(name="books")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\booktown\booksRepository")
 */
class books
{
	/**
	 * @ORM\ManyToOne(targetEntity="subjects", inversedBy="books")
	 * @ORM\JoinColumn(name="subject_id", referencedColumnName="id")
	 */
	protected $subjects;
	protected $authors;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="author_id", type="integer")
     */
    private $authorId;

    /**
     * @var integer
     *
     * @ORM\Column(name="subject_id", type="integer")
     */
    private $subjectId;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the book covers as a JPG file.")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $poster;
    
    /**
     * @var string
     *
     * @ORM\Column(name="overview", type="text")
     */
    private $overview;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return books
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set authorId
     *
     * @param integer $authorId
     * @return books
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;

        return $this;
    }

    /**
     * Get authorId
     *
     * @return integer 
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Set subjectId
     *
     * @param integer $subjectId
     * @return books
     */
    public function setSubjectId($subjectId)
    {
        $this->subjectId = $subjectId;

        return $this;
    }

    /**
     * Get subjectId
     *
     * @return integer 
     */
    public function getSubjectId()
    {
        return $this->subjectId;
    }

    /**
     * Set poster
     *
     * @param string $poster
     * @return books
     */
    public function setPoster($poster)
    {
        $this->poster = $poster;

        return $this;
    }

    /**
     * Get poster
     *
     * @return string 
     */
    public function getPoster()
    {
        return $this->poster;
    }

    /**
     * Get overview
     *
     * @return string
     */
    public function getOverview() {
    	return $this->overview;
    }
    
    /**
     * Set overview
     *
     * @return string
     */
    public function setOverview($overview) {
    	
    	$this->overview = $overview;

    	return $this;
    }
}
