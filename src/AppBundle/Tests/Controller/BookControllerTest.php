<?php
// src/AppBundle/Tests/Util/BookControllerTest.php
namespace AppBundle\Tests\Util;

use AppBundle\Controller\BookController;

class BookControllerTest extends \PHPUnit_Framework_TestCase
{
	public function testAdd()
	{
		$calc = new BookController();
		$result = $calc->add(30, 12);

		// assert that your calculator added the numbers correctly!
		$this->assertEquals(42, $result);
	}
}